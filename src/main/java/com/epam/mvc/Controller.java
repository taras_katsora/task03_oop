package com.epam.mvc;

import com.epam.company.AviaCompany;
import com.epam.planes.Plane;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;


public class Controller {
    private String mainText =
            "Please select option:\n"
                    +
                    "0) quit\n"
                    +
                    "1) generate planes\n"
                    +
                    "2) show number of planes\n"
                    +
                    "3) show total weight\n"
                    +
                    "4) show total capacity\n";

    private View mainView;
    private static AviaCompany model;

    private Scanner scan;
    private int numberPlanes;
    public Controller() {
        mainView = new View();
        scan = new Scanner(System.in);
        model = new AviaCompany();
    }

    public void start() {
        mainView.displayText(mainText);
        System.out.println("Enter number of planes: ");
        int numberPlanes = scan.nextInt();
        System.out.println("Now chose one of the meny points: ");
        String c = scan.next("[0-4]");
        while (!c.equals("0")) {
            switch (c) {
                case "0": {
                    return;
                }
                case "1":
                    generatePlanes(numberPlanes);
                    break;
                case "2": {
                    for (Plane plane : getPlanesSorted()) {
                        mainView.displayPlane(plane);
                    }
                    break;
                }
                case "3": {
                    mainView.displayText("Total weight: " + getTotalWeight());
                    break;
                }
                case "4": {
                    mainView.displayText("Total capacity: " + getTotalPlanesCapacity());
                    break;
                }

                default: {
                    System.out.println("Please insert numbers in range[0-4]!");
                    break;
                }
            }
            mainView.displayText(mainText);
            c = scan.next("[0-4]");
        }
    }

    public void generatePlanes(int size) {
        model.generatePlanes(size);
    }

    public  int getTotalPlanesCapacity() {
        int total = 0;
        for (Plane plane : model.getPlanes()) {
            total += plane.getNumberOfPlaces();
        }

        return total;
    }

    public int getTotalWeight() {
        int total =0;
        for (Plane plane : model.getPlanes()) {
            total += plane.getLiftingWeight();
        }
        return total;
    }

    public List<Plane> getPlanesSorted() {
        List<Plane> sorted = new ArrayList<>(model.getPlanes());
        Collections.sort(sorted);
        return sorted;
    }

    public List<Plane> getPlanesSorted(int from, int to) {
        List<Plane> result = new ArrayList<>();
        for (Plane plane : model.getPlanes()) {
            if (plane.getConsumption() >= from && plane.getConsumption() <= to){
                result.add(plane);
            }

        }

        return result;
    }
}
