package com.epam.mvc;

import com.epam.planes.Plane;

public class View {

    public void displayPlane(Plane plane) {
        System.out.println(plane.toString());
    }

    public void displayText(String mainText) {
        System.out.println(mainText);
    }
}
