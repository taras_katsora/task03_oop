package com.epam.planes;

public abstract class Plane implements Comparable<Plane> {
    private String model;
    private int numberOfPlaces;
    private int liftingWeight;
    private int maxDistance;
    private  int consumption;

    public Plane(String model, int numberOfPlaces, int liftingWeight, int maxDistance, int consumption) {
        this.model = model;
        this.numberOfPlaces = numberOfPlaces;
        this.liftingWeight = liftingWeight;
        this.maxDistance = maxDistance;
        this.consumption = consumption;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getNumberOfPlaces() {
        return numberOfPlaces;
    }

    public void setNumberOfPlaces(int numberOfPlaces) {
        this.numberOfPlaces = numberOfPlaces;
    }

    public int getLiftingWeight() {
        return liftingWeight;
    }

    public void setLiftingWeight(int liftingWeight) {
        this.liftingWeight = liftingWeight;
    }

    public int getMaxDistance() {
        return maxDistance;
    }

    public void setMaxDistance(int maxDistance) {
        this.maxDistance = maxDistance;
    }

    public int getConsumption() {
        return consumption;
    }

    public void setConsumption(int consumption) {
        this.consumption = consumption;
    }

    @Override
    public int compareTo(Plane o) {
        return this.consumption - o.consumption;
    }

    @Override
    public String toString() {
        return "Plane{" +
                "model='" + model + '\'' +
                ", numberOfPlaces=" + numberOfPlaces +
                ", liftingWeight=" + liftingWeight +
                ", maxDistance=" + maxDistance +
                ", consumption=" + consumption +
                '}';
    }

}
