package com.epam.planes;

public class PassengerPlane extends Plane {
    private int businessClassCapacity;

    public PassengerPlane(String model, int numberOfPlaces, int liftingWeight, int maxDistance, int businessClassCapacity, int consumption) {
        super(model, numberOfPlaces, liftingWeight, maxDistance, consumption);
        this.businessClassCapacity = businessClassCapacity;
    }

    public int getBusinessClassCapacity() {
        return businessClassCapacity;
    }

    public void setBusinessClassCapacity(int businessClassCapacity) {
        this.businessClassCapacity = businessClassCapacity;
    }

    @Override
    public String toString() {
        return "PassengerPlane{" +
                "businessClassCapacity=" + businessClassCapacity +
                "} " + super.toString();
    }

}
