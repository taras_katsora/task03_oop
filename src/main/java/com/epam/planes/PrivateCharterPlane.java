package com.epam.planes;

public class PrivateCharterPlane extends Plane {
    private int luggagePlaces;

    public PrivateCharterPlane(String model, int numberOfPlaces, int liftingWeight, int maxDistance, int luggagePlaces, int consumption) {
        super(model, numberOfPlaces, liftingWeight, maxDistance, consumption);
        this.luggagePlaces = luggagePlaces;
    }

    public int getLuggagePlaces() {
        return luggagePlaces;
    }

    public void setLuggagePlaces(int luggagePlaces) {
        this.luggagePlaces = luggagePlaces;
    }

    @Override
    public String toString() {
        return "PrivateCharterPlane{" +
                "luggagePlaces=" + luggagePlaces +
                "} " + super.toString();
    }

}
