package com.epam.planes;

public class CargoPlane extends Plane {
    private int cargoCapacity;

    public CargoPlane(String model, int liftingWeight, int maxDistance, int cargoCapacity, int consumption) {
        super(model, 0, liftingWeight, maxDistance, consumption);
        this.cargoCapacity = cargoCapacity;
    }

    public int getCargoCapacity() {
        return cargoCapacity;
    }

    public void setCargoCapacity(int cargoCapacity) {
        this.cargoCapacity = cargoCapacity;
    }

    @Override
    public String toString() {
        return "CargoPlane{" +
                "cargoCapacity=" + cargoCapacity +
                "} " + super.toString();
    }

}