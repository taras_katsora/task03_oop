package com.epam.company;

import com.epam.planes.CargoPlane;
import com.epam.planes.PassengerPlane;
import com.epam.planes.Plane;
import com.epam.planes.PrivateCharterPlane;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;



public class AviaCompany {
    private Random random = new Random(System.currentTimeMillis());

    private List<String> models = Arrays.asList("A330", "A380", "A385", "A330MAX", "A375MAX", "B747", "B737", "B787", "B777");
    private List<Plane> planes = new ArrayList<>();

    public void addPlane(Plane plane) {
        planes.add(plane);
    }

    public List<Plane> getPlanes() {
        return planes;
    }

    public void generatePlanes(int size) {
            planes.clear();


            for (int i = 0; i < size; i++) {
                int type = random.nextInt(3);
                switch (type) {
                    case 0: {
                        planes.add(generateCargoPlane());
                        break;
                    }
                    case 1: {
                        planes.add(generatePassengerPlane());
                        break;
                    }
                    case 2: {
                        planes.add(generatePrivateCharterPlane());
                        break;
                    }
                    default:
                        throw new IllegalArgumentException();
                }
            }
        }

    private Plane generatePrivateCharterPlane() {
        String model = getModel();
        return new PrivateCharterPlane(model, random.nextInt(16), random.nextInt(2500),
                random.nextInt(4500), random.nextInt(25), random.nextInt(75));
    }

    private Plane generatePassengerPlane() {
        String model = getModel();
        return new PassengerPlane(model, random.nextInt(16), random.nextInt(2500),
                random.nextInt(4500), random.nextInt(25), random.nextInt(75));
    }

    private Plane generateCargoPlane() {
        String model = getModel();
        return new CargoPlane(model, random.nextInt(16),random.nextInt(2500),
                random.nextInt(4500), random.nextInt(25));
    }

    private String getModel() {
            return models.get(random.nextInt(models.size()));

    }
}